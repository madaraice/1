﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTCB
{
    class Parser
    {
        public static List<byte> getValue(string stringData)
        {
            var splitedData = stringData.Split(',');
            var data = new List<byte>(splitedData.Length);

            foreach (var byteString in splitedData.Take(splitedData.Length - 1))
            {
                byte[] temp = StringToByteArrayFastest(byteString.Trim().Replace("0x", string.Empty));
                data.AddRange(temp);
            }
            return data;
        }

        public static byte[] StringToByteArrayFastest(string hex)
        {
            if (hex.Length % 2 == 1)
                throw new Exception("The binary key cannot have an odd number of digits");

            byte[] arr = new byte[hex.Length >> 1];

            for (int i = 0; i < hex.Length >> 1; ++i)
            {
                arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
            }

            return arr;
        }

        public static int GetHexVal(char hex)
        {
            int val = (int)hex;
            //For uppercase A-F letters:
            //return val - (val < 58 ? 48 : 55);

            //For lowercase a-f letters:
            return val - (val < 58 ? 48 : 87);

            //Or the two combined, but a bit slower:
            //return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }
    }
}
