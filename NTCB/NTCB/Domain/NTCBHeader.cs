﻿namespace NTCB.Domain
{
    class NTCBHeader
    {
        /// <summary>
        /// Преамбула
        /// </summary>
        public string Preambula { get; set; }

        /// <summary>
        /// Идентификатор получателя
        /// </summary>
        public int IDr { get; set; }

        /// <summary>
        /// Идентификатор отправителя
        /// </summary>
        public int IDs { get; set; }

        /// <summary>
        /// Количество байт данных n
        /// </summary>
        public int CountDataN { get; set; }

        /// <summary>
        /// Контрольная сумма CSd
        /// </summary>
        public int CSd { get; set; }

        /// <summary>
        /// Контрольная сумма CSp
        /// </summary>
        public int CSp { get; set; }
    }
}
