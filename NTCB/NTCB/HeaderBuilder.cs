﻿using NTCB.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTCB
{
    class HeaderBuilder
    {
        private string _preambula;
        private int _idR;
        private int _idS;
        private int _countDataN;
        private int _CSd;
        private int _CSp;

        public HeaderBuilder SetPreambula(byte[] preambula)
        {
            if (preambula == null) throw new ArgumentNullException(nameof(preambula));
            if (preambula.Length != 4) throw new ArgumentException(nameof(preambula));

            _preambula = Encoding.ASCII.GetString(preambula);
            return this;
        }

        public HeaderBuilder SetIDr(byte data)
        {
            string idR = Encoding.ASCII.GetString(new[] { data });
            _idR = int.Parse(idR);
            return this;
        }

        public HeaderBuilder SetIDs(byte data)
        {
            string idS = Encoding.ASCII.GetString(new[] { data });
            _idS = int.Parse(idS);
            return this;
        }

        public HeaderBuilder SetCountDataN(byte data)
        {
            string countData = Encoding.ASCII.GetString(new[] { data });
            _countDataN = int.Parse(countData);
            return this;
        }

        public HeaderBuilder SetControlSumm(byte SumD, byte SumP)
        {
            string CSd = Encoding.ASCII.GetString(new[] { SumD });
            _CSd = int.Parse(CSd);

            string CSp = Encoding.ASCII.GetString(new[] { SumP });
            _CSp = int.Parse(CSp);

            if (_CSd == _CSp)
                return this;
            else
                throw new Exception("Контрольная сумма не совпадает!");
        }

        public NTCBHeader Build()
        {
            return new NTCBHeader()
            {
                Preambula = _preambula,
                IDr = _idR,
                IDs = _idS,
                CountDataN = _countDataN,
                CSd = _CSd,
                CSp = _CSp
            };
        }
    }
}
