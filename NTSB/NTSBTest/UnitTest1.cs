﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTSB.Domain;
using NTSB;

namespace NTSBTest
{
    [TestClass]
    public class NTSB_Test
    {
        [TestMethod]
        public void NTCBbuilder_Build_Test()
        {
            var expected = new NTSBDataMessage()
            {
                Preambula = "@NTC",
                IDr = 8,
                IDs = 7
            };
            var actual = new NTCBbuilder()
                .SetPreambula(new byte[] { 64, 78, 84, 67 })
                //.SetIDr(8)
                //.SetIDs(7)
                .Build();

            Assert.AreEqual(expected, actual);
        }
    }
}
