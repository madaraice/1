﻿using System;
using NTSB.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTSB
{
    public class NTCBbuilder
    {
        private string _preambula;
        private int _idR;
        private int _idS;

        public NTCBbuilder SetPreambula(byte[] preambula)
        {
            if (preambula == null) throw new ArgumentNullException(nameof(preambula));
            if (preambula.Length != 4) throw new ArgumentException(nameof(preambula));

            _preambula = Encoding.ASCII.GetString(preambula);
            return this;
        }

        public NTCBbuilder SetIDr(byte data)
        {
            string idR = Encoding.ASCII.GetString(new[] { data });
            _idR = int.Parse(idR);
            return this;
        }

        public NTCBbuilder SetIDs(byte data)
        {
            string idS = Encoding.ASCII.GetString(new[] { data });
            _idS = int.Parse(idS);
            return this;
        } 

        public NTSBDataMessage Build()
        {
            return new NTSBDataMessage()
            {
                Preambula = _preambula,
                IDr = _idR,
                IDs = _idS
            };
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            string stringData = @" 0x40, 0x4e, 0x54, 0x43, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x13, 0x00, 0xfb, 0xf0,
             0x2a, 0x3e, 0x46, 0x4c, 0x45, 0x58, 0xb0, 0x0a, 0x0a, 0x45, 0xe1, 0xec, 0x00, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00,";

            var splitedData = stringData.Split(',');
            var data = new List<byte>(splitedData.Length);


            foreach (var byteString in splitedData.Take(splitedData.Length - 1))
            {
                byte[] temp = StringToByteArrayFastest(byteString.Trim().Replace("0x", string.Empty));
                data.AddRange(temp);
            }

            //var preambula = NTCB.GetPreambula(data);
            //var idr = NTCB.GetIDr(data);
        }

        public static byte[] StringToByteArrayFastest(string hex)
        {
            if (hex.Length % 2 == 1)
                throw new Exception("The binary key cannot have an odd number of digits");

            byte[] arr = new byte[hex.Length >> 1];

            for (int i = 0; i < hex.Length >> 1; ++i)
            {
                arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
            }

            return arr;
        }

        public static int GetHexVal(char hex)
        {
            int val = (int)hex;
            //For uppercase A-F letters:
            //return val - (val < 58 ? 48 : 55);

            //For lowercase a-f letters:
            return val - (val < 58 ? 48 : 87);

            //Or the two combined, but a bit slower:
            //return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }
    }
}
