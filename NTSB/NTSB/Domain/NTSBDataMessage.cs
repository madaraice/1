﻿namespace NTSB.Domain
{
    /// <summary>
    /// TODO дать комментарий
    /// </summary>
    public class NTSBDataMessage
    {
        /// <summary>
        /// Преамбула
        /// </summary>
        public string Preambula { get; set; }
        /// <summary>
        /// Идентификатор получателя
        /// </summary>
        public int IDr { get; set; }
        /// <summary>
        /// Идентификатор отправителя
        /// </summary>
        public int IDs { get; set; }
    }
}
